import { combineReducers } from 'redux'
import {reducer as formReducer} from 'redux-form' 

import TabReducer from '../common/tab/tabReducer'
import CustomerBaseReducer from '../customerBase/customerBaseReducer'


const rootReducer = combineReducers({
    tab: TabReducer,
    customerBase: CustomerBaseReducer,
    form: formReducer,
})

export default rootReducer