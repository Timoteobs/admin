import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import ContenteHeader from '../common/template/contentHeader'
import Content from '../common/template/content'
import Tabs from '../common/tab/tabs'
import TabsHeader from '../common/tab/tabsHeader'
import TabsContent from '../common/tab/tabsContent'
import TabHeader from '../common/tab/tabHeader'
import TabContent from '../common/tab/tabContent'
import {selectTab, showTabs} from '../common/tab/tabActions'

class SalesBase extends Component {

    componentWillMount(){
        this.props.selectTab('tabCreateS')
        this.props.showTabs('tabListS', 'tabCreateS')
    }
    
    render(){
        return(
            <div>
                <ContenteHeader title='Manutenção de Vendas'/>
                <Content>
                    <Tabs>
                        <TabsHeader>
                            <TabHeader label='listar' icon='bars' target='tabListS'/>
                            <TabHeader label='Incluir' icon='plus' target='tabCreateS'/>
                            <TabHeader label='Alterar' icon='pencil' target='tabUpdateS'/>
                            <TabHeader label='Excluir' icon='trash-o' target='tabDeleteS'/>
                        </TabsHeader>
                        <TabsContent>
                            <TabContent id='tabListS'><h1>Lista</h1></TabContent>
                            <TabContent id='tabCreateS'><h1>Adicionar</h1></TabContent>
                            <TabContent id='tabUpdateS'><h1>Alterar</h1></TabContent>
                            <TabContent id='tabDeleteS'><h1>Excluir</h1></TabContent>
                        </TabsContent>
                    </Tabs>
                </Content>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({selectTab, showTabs}, dispatch)
export default connect(null, mapDispatchToProps) (SalesBase)