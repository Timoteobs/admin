import axios from 'axios'
const BASE_URL = ''

export function getList(){
    const request = axios.get(`${BASE_URL}/salesBase`)
    return{
        type: 'SALES_BASE_FETCHED',
        payload: request
    }
}