import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import ContenteHeader from '../common/template/contentHeader'
import Content from '../common/template/content'
import Tabs from '../common/tab/tabs'
import TabsHeader from '../common/tab/tabsHeader'
import TabsContent from '../common/tab/tabsContent'
import TabHeader from '../common/tab/tabHeader'
import TabContent from '../common/tab/tabContent'
import {selectTab, showTabs} from '../common/tab/tabActions'
import ProductBaseList from './productBaseList'

class ProductBase extends Component {
    
    componentWillMount(){
        this.props.selectTab('tabListP')
        this.props.showTabs('tabListP', 'tabCreateP')
    }

    render(){
        return(
            <div>
                <ContenteHeader title='Manutenção de Produtos'/>
                <Content>
                    <Tabs>
                        <TabsHeader>
                            <TabHeader label='listar' icon='bars' target='tabListP'/>
                            <TabHeader label='Incluir' icon='plus' target='tabCreateP'/>
                            <TabHeader label='Alterar' icon='pencil' target='tabUpdateP'/>
                            <TabHeader label='Excluir' icon='trash-o' target='tabDeleteP'/>
                        </TabsHeader>
                        <TabsContent>
                            <TabContent id='tabListP'><h1></h1>
                                <ProductBaseList/>
                            </TabContent>
                            <TabContent id='tabCreateP'><h1>Adicionar</h1></TabContent>
                            <TabContent id='tabUpdateP'><h1>Alterar</h1></TabContent>
                            <TabContent id='tabDeleteP'><h1>Excluir</h1></TabContent>
                        </TabsContent>
                    </Tabs>
                </Content>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({selectTab, showTabs}, dispatch)
export default connect(null, mapDispatchToProps) (ProductBase)