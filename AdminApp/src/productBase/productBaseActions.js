import axios from 'axios'
const BASE_URL = ''

export function getList(){
    const request = axios.get(`${BASE_URL}/productBase`)
    return{
        type: 'PRODUCT_BASE_FETCHED',
        payload: request
    }
}