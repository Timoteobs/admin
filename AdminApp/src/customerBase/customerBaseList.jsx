import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { getList } from './costomerBaseActions'

class CustomerBaseList extends Component{

    componentDidMount(){
        this.props.getList()
    }

    renderRows(){
        const list = this.props.list || []
        return list.map(cb => (
            <tr key={bc._id}>
                <td>{cb.name}</td>
                <td>{cb.cpf}</td>
                <td>{cb.email}</td>
                <td>{cb.phone}</td>
            </tr>
        ))
    }

    render(){
        return(
            <div>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>Nome do Cliente</th>
                            <th>CPF </th>
                            <th>Email</th>
                            <th>Telefone</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderRows()}
                    </tbody>
                </table>
            </div>
        )
    }

}

const mapStateToProps = state => ({list: state.customerBase.list})
const mapDispatchToProps = dispatch => bindActionCreators({getList}, dispatch)
export default connect( mapStateToProps, mapDispatchToProps) (CustomerBaseList)