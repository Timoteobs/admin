import axios from 'axios'
const BASE_URL = ''

export function getList(){
    const request = axios.get(`${BASE_URL}/customerBase`)
    return{
        type: 'CUSTOMER_BASE_FETCHED',
        payload: request
    }
}